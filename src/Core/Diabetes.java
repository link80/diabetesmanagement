package Core;
//	Diamond - v0.5
import java.awt.*;
import java.awt.event.*;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.*;
import javax.swing.*;

import com.itextpdf.text.DocumentException;

import GUI.*;

public class Diabetes {
	private static Diabetes main = null;
	private static int userID = 1;
	private JFrame window;
	private JPanel panel;
	private JTextArea displayResults;
	private MySQLConnection myDB;
	
	public Diabetes() throws SQLException {
		prepareGUI();
		prepareDB();
	}
	
	public static void main(String[] args) throws SQLException {
		main = new Diabetes();
		main.getAllOfBloodGlucoseLevelFromServer();
	}
	
	private void prepareGUI() {
		//	Initialisieren des Frames
		window = new JFrame("Diamond - v0.5");
		
		//	Initialisieren und aufrufen des Login-Dialogs
		//LoginDialog log = new LoginDialog(window);
		
		//	 Einstellen des Frames (nur bei erfolgreichem Login!)
		window.setSize(new Dimension(500, 500));
		window.setResizable(false);
		window.setLayout(new GridLayout(1, 2));
		
		window.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				System.out.println("Trenne Verbindung...");
				try {
					myDB.closeConnection();
					System.out.println("Verbindung erfolgreich getrennt!");
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				System.exit(0);
			}
		});
		
		//	Panel des Fensters
		panel = new JPanel();
		panel.setLayout(new FlowLayout());
		
		//	Initialisierung des Menues
		JMenuBar menuBar = new JMenuBar();
		window.setJMenuBar(menuBar);
		
		JMenu fileMenu = new JMenu("Datei");
		menuBar.add(fileMenu);
		JMenu editMenu = new JMenu("Bearbeiten");
		menuBar.add(editMenu);
		JMenu helpMenu = new JMenu("Hilfe");
		menuBar.add(helpMenu);
		
		JMenuItem saveItem = new JMenuItem("Speichern...");
		fileMenu.add(saveItem);
		JMenuItem exportItem = new JMenuItem("Export als PDF...");
		exportItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				//Erstellen der Export-Datei
				try {
					PDFHandler exportFile = new PDFHandler("firstTestOfExport.pdf");
					exportFile.setHeadline("Therapiedaten f\u00fcr die \u00e4rztliche Diagnose");
					exportFile.fillData();
					exportFile.close();
				} catch (FileNotFoundException e1) {
					JOptionPane.showOptionDialog(null, "Datei konnte nicht geschrieben werden - sie ist m\u00f6glicherweise noch ge\u00f6ffnet!", "Fehler",
									JOptionPane.DEFAULT_OPTION,
									JOptionPane.ERROR_MESSAGE, null, null, e1);
					System.out.println("Fehler beim Erstellen der PDF Datei!");
				} catch (DocumentException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		fileMenu.add(exportItem);
		JMenuItem closeItem = new JMenuItem("Beenden");
		closeItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				System.out.println("Trenne Verbindung...");
				try {
					myDB.closeConnection();
					System.out.println("Verbindung erfolgreich getrennt!");
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				System.exit(0);
			}
		});
		fileMenu.add(closeItem);
		
		displayResults = new JTextArea();
		displayResults.setEditable(false);
		window.add(displayResults);
		
		//	ANZEIGE COMBOBOX
		String[] showValues = new String[] {"Alle Werte", "Alle Bolusabgaben", "Summe Bolus", "Durchschnitt Werte"};
		JComboBox<String> showField = new JComboBox<>(showValues);
		showField.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JComboBox<String> combo = (JComboBox<String>) e.getSource();
				String selectedItem = (String) combo.getSelectedItem();
				
				try {
					if (selectedItem.equals("Alle Werte")) {
						getAllOfBloodGlucoseLevelFromServer();
					} else if (selectedItem.equals("Alle Bolusabgaben")) {
						getAllOfBolusFromServer();
					} else if (selectedItem.equals("Summe Bolus")) {
						ResultSet result = myDB.executeRead("SELECT sum(hoehe) FROM bolus WHERE userID = " + userID + ";");
						if (result.next()) {
							displayResults.setText("Bolus - Summe: " + result.getString(1));
						}
					} else if (selectedItem.equals("Durchschnitt Werte")) {
						ResultSet result = myDB.executeRead("SELECT avg(wert) FROM werte WHERE userID = " + userID + ";");
						if (result.next()) {
							displayResults.setText("Werte - Durchschnitt: " + result.getString(1));
						}
					}
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
			}
		});
		panel.add(showField);
		
		//	NEUER WERT
		BloodGlucoseLevelDialog in = new BloodGlucoseLevelDialog(window);
		JButton newBloodGlucoseLevelEntry = new JButton("Neuer Wert...");
		newBloodGlucoseLevelEntry.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				in.showDialog();
				String[] abc = in.getInput();
				if (abc != null) {
					try {
						createWert(abc[0], abc[1], userID);
						getAllOfBloodGlucoseLevelFromServer();
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
			}
		});
		panel.add(newBloodGlucoseLevelEntry);
		
		//	NEUE BOLUSABGABE
		BolusDialog bd = new BolusDialog(window);
		JButton newBolusEntry = new JButton("Neuer Bolus...");
		newBolusEntry.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				bd.showDialog();
				String[] abc = bd.getInput();
				if (abc != null) {
					try {
						createBolus(abc[0], abc[1], 1, userID);
						getAllOfBolusFromServer();
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
			}
		});
		panel.add(newBolusEntry);
		
		window.add(panel);
		window.setVisible(true);
	}
	
	private void prepareDB() throws SQLException {
		myDB = new MySQLConnection();
	}
	
	private void getAllOfBloodGlucoseLevelFromServer() throws SQLException {
		ResultSet result = myDB.executeRead("SELECT ID, zeitpunkt, wert FROM werte WHERE userID = " + userID + ";");
		while (result.next()) {
			if (!BloodGlucoseLevel.exists(result.getInt(1)))
				new BloodGlucoseLevel(result.getInt(1), result.getString(2), result.getInt(3));
		}
		BloodGlucoseLevel.refreshDisplay(displayResults);
	}
	
	private void getAllOfBolusFromServer() throws SQLException {
		ResultSet result = myDB.executeRead("SELECT ID, zeitpunkt, hoehe FROM bolus WHERE userID = " + userID + ";");
		while (result.next()) {
			if (!Bolus.exists(result.getInt(1)))
				new Bolus(result.getInt(1), result.getString(2), result.getFloat(3));
		}
		Bolus.refreshDisplay(displayResults);
	}
	
	private void createWert(String timestamp, String value, int userID) throws SQLException {
		myDB.executeUpdate("INSERT INTO werte(zeitpunkt, wert, userID) VALUES ('" + timestamp + "', " + value + ", " +  userID + ");");
	}
	private void createBolus(String timestamp, String value, int type, int userID) throws SQLException {
		myDB.executeUpdate("INSERT INTO bolus(zeitpunkt, hoehe, art, userID) VALUES ('" + timestamp + "', " + value + ", " + type + ", " +  userID + ");");
	}
}
