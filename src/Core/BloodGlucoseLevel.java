package Core;

import java.util.LinkedList;
import javax.swing.*;

public class BloodGlucoseLevel {
	public static LinkedList<BloodGlucoseLevel> list = new LinkedList<BloodGlucoseLevel>();
	public static int count;
	
	private int id;
	private String timestamp;
	private int level;
	
	public BloodGlucoseLevel(int id, String timestamp, int level) {
		this.id = id;
		this.timestamp = timestamp;
		this.level = level;
		count++;
		list.add(this);
	}
	
	public static boolean exists(int id) {
		//	Check if there is an entry with the given ID
		for (BloodGlucoseLevel b : BloodGlucoseLevel.list) {
			if (b.id == id)
				return true;
		}
		return false;
	}
	
	public static void refreshDisplay(JTextArea destination) {
		destination.setText(null);
		for (BloodGlucoseLevel b : BloodGlucoseLevel.list)
			destination.append(" " + b.getTimestamp() + " - " + b.getLevel() + "\n");
	}
	
	public String getTimestamp() {
		return timestamp;
	}
	
	public int getLevel() {
		return level;
	}
}
