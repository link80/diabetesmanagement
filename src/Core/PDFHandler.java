package Core;

import java.io.*;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import com.itextpdf.text.pdf.draw.*;

//Klasse zum Handlen der PDF-Export Funktionen
public class PDFHandler {
	private Document mainDoc;
	private Font f;
	
	PDFHandler(String dest) throws DocumentException, IOException {
		System.out.println("Erstelle PDF Datei...");
		mainDoc = new Document(PageSize.A4);
		PdfWriter.getInstance(mainDoc, new FileOutputStream(dest));
		mainDoc.open();
		f = FontFactory.getFont("resources/fonts/FreeSans.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 12);
		//f = new Font(bf, 12);
	}
	
	public void setHeadline(String txtHeadline) throws DocumentException {
		Paragraph headline = new Paragraph(txtHeadline, f);
		DottedLineSeparator line = new DottedLineSeparator();
		line.setOffset(-2);
		line.setGap(2f);
		headline.add(line);
		mainDoc.add(headline);
	}
	
	public void fillData() throws DocumentException {
		for (BloodGlucoseLevel b : BloodGlucoseLevel.list)
			mainDoc.add(new Paragraph(b.getTimestamp() + " - " + b.getLevel() + " mg/dl"));
		DottedLineSeparator line = new DottedLineSeparator();
		line.setOffset(-2);
		line.setGap(2f);
		mainDoc.add(line);
		mainDoc.add(new Paragraph("Gesamtanzahl der Messungen: " + BloodGlucoseLevel.count));
	}
	
	public void close() {
		System.out.println("PDF Datei erfolgreich erstellt!");
		mainDoc.close();
	}
}
