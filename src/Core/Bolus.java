package Core;

import java.util.LinkedList;
import javax.swing.*;

public class Bolus {
	public static LinkedList<Bolus> list = new LinkedList<Bolus>();
	public static int count;
	
	private int id;
	private String timestamp;
	private float value;
	
	public Bolus(int id, String timestamp, float value) {
		this.id = id;
		this.timestamp = timestamp;
		this.value = value;
		count++;
		list.add(this);
	}
	
	public static boolean exists(int id) {
		//	Check if there is an entry with the given ID
		for (Bolus b : Bolus.list) {
			if (b.id == id)
				return true;
		}
		return false;
	}
	
	public static void refreshDisplay(JTextArea destination) {
		destination.setText(null);
		for (Bolus b : Bolus.list)
			destination.append(" " + b.getTimestamp() + " - " + b.getValue() + "\n");
	}
	
	public String getTimestamp() {
		return timestamp;
	}
	
	public float getValue() {
		return value;
	}
}
