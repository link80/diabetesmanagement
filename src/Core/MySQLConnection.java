package Core;
//	Klasse fuer den Aufbau einer MySQL-Verbindung
import java.sql.*;

public class MySQLConnection {
	private static Connection conn = null;
	private static Statement query = null;
	private static String dbHost = "server56.webgo24.de";
	private static String dbPort = "3306";
	private static String database = "web57_db7";
	private static String dbUser = "web57_7";
	private static String dbPassword = "diabAdmin";
	
	protected MySQLConnection() throws SQLException {
		System.out.println("Verbinde...");
		
		try {
			Class.forName("com.mysql.jdbc.Driver");
						
			//	Aufbau einer Verbindung (tatsaechlicher Verbindungsaufbau!)
			conn = DriverManager.getConnection("jdbc:mysql://" + dbHost + ":"
				+ dbPort + "/" + database + "?" + "user=" + dbUser + "&"
				+ "password=" + dbPassword);
			
			System.out.println("Verbindung erfolgreich hergestellt!");
		} catch (ClassNotFoundException e) {
			System.err.println("Treiber nicht gefunden!");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	private static Connection getInstance() throws SQLException {
		if (conn == null)
			new MySQLConnection();
		return conn;
	}
	
	public ResultSet executeRead(String txt) throws SQLException {
		conn = getInstance();
		
		if (conn != null) {
			try {
				query = conn.createStatement();
				ResultSet result = query.executeQuery(txt);
				return result;
			} catch (SQLException e) {
				e.printStackTrace();
			}
		} else {
			System.err.println("ERROR");
		}
		return null;
	}
	
	public void executeUpdate(String txt) throws SQLException {
		conn = getInstance();
		
		if (conn != null) {
			try {
				query = conn.createStatement();
				query.executeUpdate(txt);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		} else {
			System.err.println("ERROR");
		}
	}
	
	public void closeConnection() throws SQLException {
		conn = getInstance();
		
		if (query != null)
			query.close();
		if (conn != null)
			conn.close();
	}
}
