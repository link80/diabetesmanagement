package GUI;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

@SuppressWarnings("serial")
public class BolusDialog extends JDialog {
	private JLabel lblTimestamp;
	private JLabel lblValue;
	private PlaceholderTextField txtTimestamp;
	private PlaceholderTextField txtValue;
	private JButton btnOK;
	private JButton btnCancel;
	private String[] result = null;
	
	public BolusDialog(JFrame parent) {
		super(parent);
		
		lblTimestamp	= new JLabel("Wann:");
		txtTimestamp	= new PlaceholderTextField();
		txtTimestamp.setPlaceholder("z.B. 2016-08-10 15:31:23");
		lblValue 		= new JLabel("Hoehe:");
		txtValue 		= new PlaceholderTextField();
		txtValue.setPlaceholder("z.B. 12.4");
		
		btnOK = new JButton("OK");
		btnOK.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setInput();
				setVisible(false);
				dispose();
			}
		});
		btnCancel = new JButton("Abbrechen");
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				result = null;
				setVisible(false);
				dispose();
			}
		});
		
		this.setTitle("Neue Bolusabgabe...");
		this.setLayout(new GridLayout(3, 2));
		this.add(lblTimestamp);
		this.add(txtTimestamp);
		this.add(lblValue);
		this.add(txtValue);
		this.add(btnOK);
		this.add(btnCancel);
		
		this.setModalityType(java.awt.Dialog.ModalityType.APPLICATION_MODAL);
		this.pack();
	}
	
	public void showDialog() {
		this.setVisible(true);
	}
	
	public String[] getInput() {
		return result;
	}
	
	public void setInput() {
		result = new String[2];
		result[0] = txtTimestamp.getText();
		result[1] = txtValue.getText();
	}
}
