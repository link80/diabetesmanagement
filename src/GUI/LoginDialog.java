package GUI;
import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
import javax.swing.text.*;

public class LoginDialog extends JDialog {
	private JLabel lblUsername;
	private JLabel lblPassword;
	private JTextField txtUsername;
	private JPasswordField txtPassword;
	private JButton btnLogin;
	private JButton btnClose;
	
	public LoginDialog(JFrame parent) {
		super(parent);
		
        InputVerifier verifier = new InputVerifier() {
            public boolean verify(JComponent input) {
                JTextComponent textComponent = (JTextComponent) input;
                return !textComponent.getText().trim().equals("");
            }
        };
		
		lblUsername	= new JLabel("Username: ");
		txtUsername	= new JTextField(20);
		txtUsername.setInputVerifier(verifier);
		
		lblPassword	= new JLabel("Passwort: ");
		txtPassword	= new JPasswordField(20);
		txtPassword.setInputVerifier(verifier);
		
		btnLogin	= new JButton("Login");
		btnLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//TODO
				setVisible(false);
				dispose();
			}
		});
		btnClose	= new JButton("Beenden");
		btnClose.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//TODO
				System.exit(0);
			}
		});
		
		this.setTitle("Login");
		this.setLayout(new GridLayout(3, 2));
		this.add(lblUsername);
		this.add(txtUsername);
		this.add(lblPassword);
		this.add(txtPassword);
		this.add(btnLogin);
		this.add(btnClose);
		
		this.setModalityType(java.awt.Dialog.ModalityType.APPLICATION_MODAL);
		this.pack();
		this.setVisible(true);
	}
	
	private void login(String username, String password) throws SecurityException {
		//TODO
		if (!(username.equals("root") && password.equals("toor"))) {
			throw new SecurityException("Falsche Login-Daten!");
		}
	}
}
